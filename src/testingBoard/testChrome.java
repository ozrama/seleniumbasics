package testingBoard;

import org.openqa.selenium.opera.OperaDriver;

public class testChrome {

	public static void main(String[] args) {

		/*
		 * String pathToChromeDriver = ".//chromeDriver//chromedriver.exe";
		 * System.setProperty("webdriver.chrome.driver", pathToChromeDriver);
		 * 
		 * ChromeDriver Chrom1 = new ChromeDriver();
		 * Chrom1.get("https://www.testinst.testing-board.com/");
		 */

		String pathToOperaDriver = ".//operaDriver//operadriver.exe";
		System.setProperty("webdriver.opera.driver", pathToOperaDriver);

		OperaDriver opera1 = new OperaDriver();
		opera1.get("https://www.testinst.testing-board.com/");
	}
}
