package testingBoard;

import org.openqa.selenium.firefox.FirefoxDriver;

public class test_basic_auth_website {

	public static void main(String[] args) {

		FirefoxDriver fox1 = new FirefoxDriver();
		get_TBcom_with_basic_auth(fox1);
	}

	public static void get_TBcom_with_basic_auth(FirefoxDriver foxDr) {
		String URL = "http://" + "UsernameHTTPAuth" + ":" + "PasswortHTTPAuth" + "@" + "www.testinst.testing-board.com";
		foxDr.get(URL);
	}

}
